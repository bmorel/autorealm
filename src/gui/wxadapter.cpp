/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#include "wxadapter.h"
#include <string>
#include <boost/program_options.hpp>

namespace po=boost::program_options;
namespace fs=boost::filesystem;

std::istream& operator>>(std::istream& in, ItemKind& kind)
{
	std::string token;
	in >> token;
	if(token == "separator")
		kind= ItemKind::SEPARATOR;
	else if(token == "normal")
		kind= ItemKind::NORMAL;
	else if(token == "check")
		kind= ItemKind::CHECK;
	else if(token == "radio")
		kind= ItemKind::RADIO;
	else
		throw po::validation_error(po::validation_error::invalid_option, token, "kind");
	return in;
}
