/**********************************************************************************
 *autorealm - A vectorized graphic editor to create maps, mostly for RPG games    *
 *Copyright (C) 2012-2013 Morel Bérenger                                          *
 *                                                                                *
 *This file is part of autorealm.                                                 *
 *                                                                                *
 *    autorealm is free software: you can redistribute it and/or modify           *
 *    it under the terms of the GNU Lesser General Public License as published by *
 *    the Free Software Foundation, either version 3 of the License, or           *
 *    (at your option) any later version.                                         *
 *                                                                                *
 *    autorealm is distributed in the hope that it will be useful,                *
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *    GNU Lesser General Public License for more details.                         *
 *                                                                                *
 *    You should have received a copy of the GNU Lesser General Public License    *
 *    along with autorealm.  If not, see <http://www.gnu.org/licenses/>.          *
 **********************************************************************************/

#ifndef WXADAPTER_H
#define WXADAPTER_H

#include <istream>
#include <wx/menu.h>
#include <boost/filesystem.hpp>
#include <tree.h>

enum ItemKind
{
	SEPARATOR=wxITEM_SEPARATOR,
	NORMAL=wxITEM_NORMAL,
	CHECK=wxITEM_CHECK,
	RADIO=wxITEM_RADIO
};

/**
 * \brief parse a stream to extract an option of type ItemKind
 * \pre input stream contains a valid ItemKind option
 * \post kind contains a value according to the stream's value
 */
std::istream& operator>>(std::istream& in, ItemKind& kind);

namespace fs=boost::filesystem;
/**
 * \brief extract information from configuration files and folders and build a tree with them
 * \return a tree of NodeType instances
 * \pre origin is an existing folder
 * \invariant disk content is not changed
 *
 */
template <typename NodeType>
Node<NodeType> createTree(fs::path const& origin)
{
	if(!fs::exists(origin))
		throw std::runtime_error(origin.string()+"does not exists\n");

	Node<NodeType> f(origin);
	for(auto it=fs::directory_iterator(origin);it!=fs::directory_iterator();++it)
	{
		if(fs::is_directory(*it))
			f.push_back(createTree<NodeType>(*it));
		else if(it->path().filename().string()!=origin.filename().string())
			f.push_back(Leaf<NodeType>(NodeType(*it)));
	}
	return f;
}

#endif
