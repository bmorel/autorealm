#include <gui/renderwindow.h>

#include <wx/menu.h>

#include "polydrawer.h"
#include "renderer.h"

ID PolyDrawer::m_menuIds[3];
wxMenu *PolyDrawer::m_menu(nullptr);

PolyDrawer::PolyDrawer(RenderWindow *window, std::unique_ptr<Renderer> r, Render::TagList const &tags)
:Drawer(window, std::move(r), tags)
{
	if(!m_menu)
	{
		m_menu=new wxMenu();

		m_menu->Append(m_menuIds[0], wxT("Create &Open Figure"));
		m_menu->Append(m_menuIds[1], wxT("Create &Closed Figure"));
		m_menu->AppendSeparator();
		m_menu->Append(m_menuIds[2], wxT("Suppress this menu and use &Shift for Closed Figures"));
	}
}

PolyDrawer::PolyDrawer(PolyDrawer const& other)
:Drawer(other)
{
}

PolyDrawer::~PolyDrawer(void)throw()
{
	removeEventManager();
}

void PolyDrawer::installEventManager(void) throw()
{
	bindMouse();
	m_target->Bind(wxEVT_COMMAND_MENU_SELECTED, &PolyDrawer::finalizeShape, this, m_menuIds[0], m_menuIds[1]);
	m_target->Bind(wxEVT_COMMAND_MENU_SELECTED, &PolyDrawer::switchShift, this, m_menuIds[2]);
}

void PolyDrawer::removeEventManager(void) throw()
{
	unbindMouse();
	m_target->Unbind(wxEVT_COMMAND_MENU_SELECTED, &PolyDrawer::finalizeShape, this, m_menuIds[0], m_menuIds[1]);
	m_target->Unbind(wxEVT_COMMAND_MENU_SELECTED, &PolyDrawer::switchShift, this, m_menuIds[2]);
}

void PolyDrawer::finalizeShape(wxCommandEvent &event)
{
	unbindMouse();

	m_shape.pop();
	if(event.GetId()==m_menuIds[1])//user asked for a closed shape
		m_shape.close();
	m_target->push_back(m_shape);
	render();
	m_shape.clear();

	bindMouse();
}

void PolyDrawer::contextMenu(wxContextMenuEvent &event)
{
	if(m_useShift)
	{
		wxCommandEvent ev(wxEVT_NULL,m_menuIds[0]);
		if(::wxGetKeyState(WXK_SHIFT))
			ev.SetId(m_menuIds[1]);
		finalizeShape(ev);
	}
	else
	{
		wxPoint point = event.GetPosition();

		if(-1 == point.x && -1 == point.y) //from keyboard ?
		{
			assert(0);
			///\todo implement popup's menu's position when user use the context menu key
			///\fixme it seem assert(0) is never executed. Guess that it is because I did not used the application pointer?
		}
		else
			point = m_target->ScreenToClient(point);

		m_target->PopupMenu(m_menu, point);
	}
}

void PolyDrawer::firstPoint(wxMouseEvent &event)
{
	unbindMouse();
	createShape();
	addVertex(Render::Point(event.GetX(),event.GetY(),0));
	addPoint(event);
	bindMouse();
}

void PolyDrawer::secondPoint(wxMouseEvent &event)
{
	unbindMouse();
	addPoint(event);
	bindMouse();
}

void PolyDrawer::switchShift(wxCommandEvent &event)
{
	m_useShift=!m_useShift;
}

void PolyDrawer::bindMouse(void)
{
	switch(m_shape.size())
	{
		case 0:
			m_target->Bind(wxEVT_LEFT_DOWN, &PolyDrawer::firstPoint, this);
			break;
		case 1:
			throw std::logic_error("m_shape should never have only one point, since mouse tracking is not an option");
		case 2:
			m_target->Bind(wxEVT_LEFT_DOWN, &PolyDrawer::secondPoint, this);
			m_target->Bind(wxEVT_MOTION, &PolyDrawer::moveMouse, this);
			break;
		default:
			m_target->Bind(wxEVT_LEFT_DOWN, &PolyDrawer::addPoint, this);
			m_target->Bind(wxEVT_MOTION, &PolyDrawer::moveMouse, this);
			m_target->Bind(wxEVT_CONTEXT_MENU, &PolyDrawer::contextMenu, this);
	}
}

void PolyDrawer::unbindMouse(void)
{
	switch(m_shape.size())
	{
		case 0:
			m_target->Unbind(wxEVT_LEFT_DOWN, &PolyDrawer::firstPoint, this);
			break;
		case 1:
			throw std::logic_error("m_shape should never have only one point, since mouse tracking is not an option");
		case 2:
			m_target->Unbind(wxEVT_LEFT_DOWN, &PolyDrawer::secondPoint, this);
			m_target->Unbind(wxEVT_MOTION, &PolyDrawer::moveMouse, this);
			break;
		default:
			m_target->Unbind(wxEVT_LEFT_DOWN, &PolyDrawer::addPoint, this);
			m_target->Unbind(wxEVT_MOTION, &PolyDrawer::moveMouse, this);
			m_target->Unbind(wxEVT_CONTEXT_MENU, &PolyDrawer::contextMenu, this);
	}
}

