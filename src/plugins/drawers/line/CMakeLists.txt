FIND_PACKAGE(wxWidgets REQUIRED )
INCLUDE(${wxWidgets_USE_FILE})

FILE(GLOB_RECURSE source_files "*.cpp")

ADD_LIBRARY(line SHARED ${source_files} )
TARGET_LINK_LIBRARIES(line
	pluginengine
	${wxWidgets_LIBRARIES}
	${Pluma_LIBRARIES}
)
